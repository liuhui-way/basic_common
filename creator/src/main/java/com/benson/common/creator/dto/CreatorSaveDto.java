package com.benson.common.creator.dto;

import com.benson.common.creator.entity.Creator;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotBlank;

/**
 * 代码生成 保存Dto
 *
 * @author zhangby
 * @date 18/2/20 2:08 pm
 */
@Data
@Accessors(chain = true)
public class CreatorSaveDto {

    @NotBlank(message = "标签名称不能为空")
    private String name;

    @NotBlank(message = "数据库表名不能为空")
    private String tableName;

    private String author;

    private String outPutDir;

    @NotBlank(message = "包名不能为空")
    private String packageDir;

    private String tablePrefix;

    @NotBlank(message = "请选择文件生成类型")
    private String createFile;

    public Creator convert() {
        Creator creator = new Creator();
        BeanUtils.copyProperties(this, creator);
        return creator;
    }
}
