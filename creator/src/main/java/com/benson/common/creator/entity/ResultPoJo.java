package com.benson.common.creator.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 返回数据
 *
 * @author zhangby
 * @date 2019-05-13 12:11
 */
@Data
@Accessors(chain = true)
public class ResultPoJo<T> {

    private String code;
    private String msg;
    private T result;

    public static ResultPoJo ok() {
        return new ResultPoJo()
                .setCode("000")
                .setMsg("成功");
    }

    public static ResultPoJo ok(Object obj) {
        return new ResultPoJo()
                .setCode("000")
                .setMsg("成功")
                .setResult(obj);
    }
}
