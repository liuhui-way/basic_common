package com.benson.common.creator.entity.enums;

/**
 * 文件类型枚举
 *
 * @author zhangby
 * @date 18/2/20 1:33 pm
 */
public enum TypeFileEnum {
    /**
     * 代码生成类型
     */
    ENTITY("ENTITY", "ENTITY"),
    CONTROLLER("CONTROLLER", "CONTROLLER"),
    SERVICE("SERVICE", "SERVICE"),
    SERVICE_IMPL("SERVICE_IMPL", "SERVICE_IMPL"),
    MAPPER("DAO", "MAPPER"),
    XML("XML", "OTHER"),
    ;

    private String label;
    private String value;

    TypeFileEnum(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}
