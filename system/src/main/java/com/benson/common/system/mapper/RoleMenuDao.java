package com.benson.common.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.benson.common.system.entity.RoleMenu;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-26
 */
@Repository
public interface RoleMenuDao extends BaseMapper<RoleMenu> {

}
