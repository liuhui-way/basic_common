package com.benson.common.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.benson.common.system.entity.Menu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-27
 */
@Repository
public interface MenuDao extends BaseMapper<Menu> {

    List<Menu> getMenu4User(@Param("userId") String currentUserId);

}
