package com.benson.common.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.benson.common.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户角色表 ${aaaa}
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_user_role")
@ApiModel(value="UserRole对象", description="用户角色表")
public class UserRole extends BaseEntity<UserRole> {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户编号")
    private String userId;

    @ApiModelProperty(value = "角色编号")
    private String roleId;


    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

}
