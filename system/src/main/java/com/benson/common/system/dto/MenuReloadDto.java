package com.benson.common.system.dto;

import cn.hutool.core.util.StrUtil;
import com.benson.common.system.entity.Menu;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.function.Function;

/**
 * 初始化菜单dto
 *
 * @author zhangby
 * @date 28/11/19 12:26 pm
 */
@Data
@Accessors(chain = true)
public class MenuReloadDto {
    private String path;
    private String name;
    private String icon;
    private String target;
    private String params;
    @JsonIgnore
    private String id;
    @JsonIgnore
    private String parentId;

    private List<MenuReloadDto> children;

    /**
     * 初始化
     */
    public static Function<Menu, MenuReloadDto> init = menu ->
            new MenuReloadDto()
                    .setPath(menu.getHref())
                    .setParams(StrUtil.isNotBlank(menu.getTarget()) ? "?menuId="+ menu.getId() : null)
                    .setName(menu.getName())
                    .setIcon(menu.getIcon())
                    .setTarget(menu.getComponent())
                    .setId(menu.getId())
                    .setParentId(menu.getParentId());
}
