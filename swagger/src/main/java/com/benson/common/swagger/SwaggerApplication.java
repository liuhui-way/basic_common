package com.benson.common.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Swagger UI 服务支持
 *
 * @author zhangby
 * @date 19/2/20 4:55 pm
 */
@Controller
@SpringBootApplication
public class SwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerApplication.class, args);
    }

    @RequestMapping({"/swagger/api", "/login/swagger"})
    public String index() {
        return "swagger/index";
    }
}
