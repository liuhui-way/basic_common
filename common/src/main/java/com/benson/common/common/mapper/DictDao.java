package com.benson.common.common.mapper;

import com.benson.common.common.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-02-19
 */
@Repository
public interface DictDao extends BaseMapper<Dict> {

}
