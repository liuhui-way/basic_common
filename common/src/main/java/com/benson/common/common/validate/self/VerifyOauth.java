package com.benson.common.common.validate.self;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限验证
 * 
 * @author zhangby
 * @date 20/10/20 5:42 pm
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
//代表处理逻辑是MyConstraintValidator类
@Constraint(validatedBy = VerifyOauthValidator.class)
public @interface VerifyOauth {
    String message() default "权限不足";

    String role();

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
