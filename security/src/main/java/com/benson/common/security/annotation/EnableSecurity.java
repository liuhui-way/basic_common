package com.benson.common.security.annotation;

import com.benson.common.security.SecurityApplication;
import com.benson.common.security.config.oauth.WebSecurityConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 授权启动项
 *
 * @author zhangby
 * @date 17/2/20 4:22 pm
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({SecurityApplication.class, WebSecurityConfig.class})
public @interface EnableSecurity {

}
