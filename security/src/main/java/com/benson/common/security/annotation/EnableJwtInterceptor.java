package com.benson.common.security.annotation;

import com.benson.common.security.config.oauth.MVCConfig;
import org.springframework.context.annotation.Import;
import java.lang.annotation.*;

/**
 * jwt拦截器，默认拦截验证登录用户 token
 *
 * @author zhangby
 * @date 18/1/20 1:51 pm
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({MVCConfig.class})
public @interface EnableJwtInterceptor {

}
