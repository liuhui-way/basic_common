package com.benson.common.security.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "basic-common.security")
public class SecurityConfig {
    private String excludePath;
}

