package com.benson.common.security.annotation;

import com.benson.common.security.config.oauth.WeResourceConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfiguration;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @Description: 全生态统一的令牌认证
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/10 8:27 上午
 */
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({ java.lang.annotation.ElementType.TYPE })
@Documented
@Import({ WeResourceConfiguration.class, ResourceServerConfiguration.class })
@EnableSecurity
@Configuration
public @interface EnableResource {

}