package com.benson.common.security.config.oauth;

import com.benson.common.common.constants.Constants;
import com.benson.common.security.entity.SecurityConfig;
import com.benson.common.security.interceptor.AccessTokenFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * 资源服务器配置
 *
 * @author zhangby
 * @date 2019-06-20 15:43
 */
@Configuration
@Slf4j
@EnableResourceServer
public class WeResourceConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    private SecurityConfig securityConfig;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        String path = Constants.FILTER_EXCLUDE_PATH + "," + securityConfig;
        http
                .csrf().disable()
                .logout().disable()
                .addFilterBefore(new AccessTokenFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(path.split(",")).permitAll()
                .antMatchers("/**").authenticated()
                .and()
                .headers().frameOptions().disable();
    }


    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("oauth-client").tokenStore(tokenStore);
    }

    @Autowired
    TokenStore tokenStore;
}