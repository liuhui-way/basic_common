package com.benson.common.upload.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 文件上传 实体类
 *
 * @author zhangby
 * @date 15/1/20 6:14 pm
 */
@Data
@Accessors(chain = true)
public class UploadItem {
    private String uid;
    private String name;
    /**
     * 状态有：uploading done error removed
     */
    private String status = "done";
    private String url;
    private String thumbUrl;

    public static UploadItem error() {
        UploadItem uploadItem = new UploadItem()
                .setStatus("error");
        return uploadItem;
    }

    public static UploadItem ok() {
        return new UploadItem();
    }
}