package com.benson.common.upload.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.benson.common.common.entity.Pagination;
import com.benson.common.common.entity.ResultPoJo;
import com.benson.common.common.exception.MyBaselogicException;
import com.benson.common.upload.dto.UploadConfigDto;
import com.benson.common.upload.entity.UploadItem;
import com.benson.common.upload.service.IUploadService;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * 上传服务
 *
 * @author zhangby
 * @date 28/9/20 12:19 pm
 */
@RequestMapping("/file/upload")
@RestController
@Api(tags = "文件上传服务")
public class UploadController {

    @Autowired
    private IUploadService uploadService;

    /**
     * 文件上传服务
     *
     * @param file
     * @return
     */
    @PostMapping("/image")
    @ResponseBody
    @ApiOperation(value = "upload image", notes = "", produces = "application/json")
    public ResultPoJo<UploadItem> uploadImage(@RequestParam("file") MultipartFile file) {
        UploadItem result = uploadService.uploadImage(file);
        return ResultPoJo.ok(result);
    }

    /**
     * 文件上传服务
     *
     * @param file
     * @return
     */
    @PostMapping("/video")
    @ResponseBody
    @ApiOperation(value = "upload video", notes = "", produces = "application/json")
    public ResultPoJo<UploadItem> uploadVideo(@RequestParam("file") MultipartFile file) {
        UploadItem result = uploadService.uploadVideo(file);
        return ResultPoJo.ok(result);
    }

    /**
     * 文件上传服务
     *
     * @param file
     * @return
     */
    @PostMapping("/other/file")
    @ResponseBody
    @ApiOperation(value = "upload other file", notes = "", produces = "application/json")
    public ResultPoJo<UploadItem> uploadOtherFile(@RequestParam("file") MultipartFile file) {
        UploadItem result = uploadService.uploadOtherFile(file);
        return ResultPoJo.ok(result);
    }

    /**
     * 更新文件上传配置
     *
     * @return
     */
    @PutMapping("/config/update")
    @ApiOperation(value = "更新文件上传配置", notes = "更新文件上传配置", produces = "application/json")
    public ResultPoJo uploadConfigUpdate(@RequestBody UploadConfigDto uploadConfig) {
        uploadService.uploadConfigUpdate(uploadConfig);
        return ResultPoJo.ok();
    }

    /**
     * 查询文件上传路径
     *
     * @return
     */
    @GetMapping("/path/tree")
    @ApiOperation(value = "查询文件上传路径", notes = "查询文件上传路径", produces = "application/json")
    public ResultPoJo<List<Dict>> getFilePathTree(@RequestParam(value = "path", required = false) String path) {
        List<Dict> result = uploadService.getFilePathTree(path);
        return ResultPoJo.ok(result);
    }

    /**
     * 初始化路径
     *
     * @return
     */
    @GetMapping("/path/tree/init")
    @ApiOperation(value = "初始化路径", notes = "初始化路径", produces = "application/json")
    public ResultPoJo<List<Dict>> getFilePathTreeInit(@RequestParam(value = "path", required = false) String path) {
        List<Dict> result = uploadService.getFilePathTreeInit(path);
        return ResultPoJo.ok(result);
    }

    /**
     * 获取文件列表
     *
     * @return
     */
    @GetMapping("/file/list")
    @ApiOperation(value = "获取文件列表", notes = "获取文件列表", produces = "application/json")
    public ResultPoJo<Dict> getFileList4Page(
            Pagination pagination,
            @RequestParam(value = "path", required = false) String path,
            @RequestParam(value = "keyword", required = false) String keyword
    ) {
        Page pageParams = pagination.page();
        Dict page = Dict.create()
                .set("total", 0)
                .set("size", pageParams.getSize())
                .set("current", pageParams.getCurrent())
                .set("records", Lists.newArrayList());
        try {
            page = uploadService.getFileList4Page(pageParams, path, keyword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultPoJo.ok(page);
    }

     /**
      * 删除文件
      * @return
      */
     @DeleteMapping("/delete")
     @ApiOperation(value = "删除文件", notes = "删除文件", produces = "application/json")
     public ResultPoJo deleteFile(@RequestParam(value = "path", required = false) String path) {
         try {
             if (StrUtil.isNotBlank(path)) {
                 String fileSrc = uploadService.getUploadFileSrc();
                 for (String src : path.split(",")) {
                     FileUtil.del(fileSrc + src);
                 }
             }
         } catch (Exception e) {
             e.printStackTrace();
             throw new MyBaselogicException("文件删除失败，请稍后重试！");
         }
         return ResultPoJo.ok();
     }
}
