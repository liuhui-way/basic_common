package com.benson.common.upload.dto;

import lombok.Data;

/**
 * 文件上传对象
 *
 * @author zhangby
 * @date 20/10/20 1:43 pm
 */
@Data
public class UploadConfigDto {
    private String fileSrc;
    private String visitUrl;
    private String uploadImgUrl;
    private String uploadVideoUrl;
    private String uploadOtherUrl;
}
