package com.benson.common.upload.service;

import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.benson.common.upload.dto.UploadConfigDto;
import com.benson.common.upload.entity.UploadItem;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IUploadService {
    UploadItem uploadImage(MultipartFile file);

    void uploadConfigUpdate(UploadConfigDto uploadConfig);

    String getUploadFileSrc();

    List<Dict> getFilePathTree(String path);

    List<Dict> getFilePathTreeInit(String path);

    Dict getFileList4Page(Page page, String path, String keyword);

    UploadItem uploadVideo(MultipartFile file);

    UploadItem uploadOtherFile(MultipartFile file);
}
