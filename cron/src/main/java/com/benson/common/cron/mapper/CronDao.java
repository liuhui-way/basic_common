package com.benson.common.cron.mapper;

import com.benson.common.cron.entity.Cron;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 定时任务表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-02-24
 */
@Repository
public interface CronDao extends BaseMapper<Cron> {

}
