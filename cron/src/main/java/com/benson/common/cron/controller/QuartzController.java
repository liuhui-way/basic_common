package com.benson.common.cron.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.benson.common.common.entity.ResultPoJo;
import com.benson.common.common.exception.MyBaselogicException;
import com.benson.common.common.util.CommonUtil;
import com.benson.common.cron.entity.QuartzJobDetails;
import com.benson.common.cron.service.IQuartzService;
import com.benson.common.cron.util.QuartzUtils;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 任务调度
 *
 * @author zhangby
 * @date 11/12/19 5:02 pm
 */
@RestController
@RequestMapping("/quartz")
@Api(tags = "任务调度管理")
public class QuartzController {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private IQuartzService quartzService;


    /**
     * 获取cron表达式下次执行时间
     *
     * @return
     */
    @GetMapping("/next/cron/date")
    @ApiOperation(value = "获取cron表达式下次执行时间", notes = "获取cron表达式下次执行时间", produces = "application/json")
    public ResultPoJo<List<String>> getNextCronDate(String cron, Integer times) {
        List<String> rs = Lists.newArrayList();
        CommonUtil.notEmpty(cron).ifPresent(cronStr -> {
            rs.addAll(QuartzUtils.getNextExecTime(cronStr, Optional.ofNullable(times).orElse(5)));
        });
        return ResultPoJo.ok(CommonUtil.convers(rs, date -> date + "【 " + DateUtil.dayOfWeekEnum(DateUtil.parseDateTime(date)).toChinese() + " 】"));
    }

    /**
     * 创建定时任务
     *
     * @return
     */
    @PostMapping("/create/schedule/{jobName}")
    @ApiOperation(value = "创建定时任务", notes = "创建定时任务", produces = "application/json")
    public ResultPoJo createScheduleJob(@PathVariable String jobName) {
        QuartzUtils.createScheduleJob(scheduler, jobName);
        return ResultPoJo.ok();
    }

    /**
     * 暂停定时任务
     *
     * @return
     */
    @PostMapping("/pause/schedule/{jobName}")
    @ApiOperation(value = "暂停定时任务", notes = "暂停定时任务", produces = "application/json")
    public ResultPoJo pauseSchedule(@PathVariable String jobName) {
        QuartzUtils.pauseScheduleJob(scheduler, jobName);
        return ResultPoJo.ok();
    }

    /**
     * 恢复定时任务
     *
     * @return
     */
    @PostMapping("/resume/schedule/{jobName}")
    @ApiOperation(value = "恢复定时任务", notes = "恢复定时任务", produces = "application/json")
    public ResultPoJo resumeSchedule(@PathVariable String jobName) {
        QuartzUtils.resumeScheduleJob(scheduler, jobName);
        return ResultPoJo.ok();
    }

    /**
     * 执行一次定时任务
     *
     * @return
     */
    @PostMapping("/runOnce/schedule/{jobName}")
    @ApiOperation(value = "执行一次定时任务", notes = "执行一次定时任务", produces = "application/json")
    public ResultPoJo runOnceSchedule(@PathVariable String jobName) {
        QuartzUtils.runOnce(scheduler, jobName);
        return ResultPoJo.ok();
    }

    /**
     * 更新定时任务
     *
     * @return
     */
    @PutMapping("/update/schedule/{jobName}")
    @ApiOperation(value = "更新定时任务", notes = "更新定时任务", produces = "application/json")
    public ResultPoJo updateSchedule(@PathVariable String jobName, String cron) {
        QuartzUtils.updateScheduleJob(scheduler, jobName, cron);
        return ResultPoJo.ok();
    }

    /**
     * 删除定时任务
     *
     * @return
     */
    @DeleteMapping("/delete/schedule/{jobName}")
    @ApiOperation(value = "删除定时任务", notes = "删除定时任务", produces = "application/json")
    public ResultPoJo deleteSchedule(@PathVariable String jobName) {
        QuartzUtils.deleteScheduleJob(scheduler, jobName);
        return ResultPoJo.ok();
    }

    /**
     * 获取定时任务详情
     *
     * @return
     */
    @GetMapping("/get/schedule/{jobName}")
    @ApiOperation(value = "获取定时任务详情", notes = "获取定时任务详情", produces = "application/json")
    public ResultPoJo<QuartzJobDetails> getScheduleDetail(@PathVariable String jobName) {
        QuartzJobDetails quartzJobDetails = QuartzUtils.getQuartzJobDetails(scheduler, jobName);
        return ResultPoJo.ok(quartzJobDetails);
    }

    /**
     * 获取任务列表
     *
     * @return
     */
    @GetMapping("/get/jobs")
    @ApiOperation(value = "获取任务列表", notes = "获取任务列表", produces = "application/json")
    public ResultPoJo<List<Dict>> jobs(String keyword) throws SchedulerException {
        List<Dict> jobList = quartzService.getJobs(keyword);
        return ResultPoJo.ok(jobList);
    }

    /**
     * 获取任务列表
     *
     * @return
     */
    @GetMapping("/get/jobs/monitor")
    @ApiOperation(value = "获取任务列表", notes = "获取任务列表", produces = "application/json")
    public ResultPoJo<List<Dict>> jobsMonitor(String keyword) throws SchedulerException {
        List<Dict> jobList = quartzService.getJobsMonitor(keyword);
        return ResultPoJo.ok(jobList);
    }

     /**
      * 获取定时任务详情
      * @return
      */
     @GetMapping("/get/job/{jobName}")
     @ApiOperation(value = "获取定时任务详情", notes = "获取定时任务详情", produces = "application/json")
     public ResultPoJo<Dict> getJobByJobName(@PathVariable String jobName) {
         Dict result = quartzService.getJobByJobName(jobName);
         if (ObjectUtil.isNull(result)) {
             throw new MyBaselogicException("999", "定时任务不存在");
         }
         return ResultPoJo.ok(result);
     }
}
