package com.benson.common.cron.cron;

import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 定时任务 执行程序
 *
 * @author zhangby
 * @date 24/2/20 2:05 pm
 */
public abstract class BaseCron extends QuartzJobBean {

}