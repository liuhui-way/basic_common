package com.benson.common.cron;

import com.benson.common.creator.annotation.EnableCreatorServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 定时任务
 *
 * @author zhangby
 * @date 24/2/20 11:00 am
 */

@EnableScheduling
@EnableCreatorServer
@MapperScan("com.benson.common.cron.mapper")
@SpringBootApplication(scanBasePackages = "com.benson.common.*")
@Controller
public class CronApplication {

    public static void main(String[] args) {
        SpringApplication.run(CronApplication.class, args);
    }

    @RequestMapping({"/cron/config","/login/cron"})
    public String index() {
        return "cron/index";
    }
}
