package com.benson.common.cron.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 定时任务 配置文件
 */
@Data
@Component
public class CronCommonConfig {

    @Value("${basic-common.cron.scan-package:}")
    private String scanPackage;
}
