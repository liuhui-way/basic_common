package com.benson.common.cron.service;

import cn.hutool.core.lang.Dict;

import java.util.List;

public interface IQuartzService {
    List<Dict> getJobs(String keyword);

    Dict getJobByJobName(String jobName);

    List<Dict> getJobsMonitor(String keyword);
}
