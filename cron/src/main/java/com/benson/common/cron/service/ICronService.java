package com.benson.common.cron.service;

import com.benson.common.cron.entity.Cron;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.function.Function;

/**
 * <p>
 * 定时任务表 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-02-24
 */
public interface ICronService extends IService<Cron> {

    Function<Cron, Cron> initCron();

    Cron getCronByMark(String keyName);
}
