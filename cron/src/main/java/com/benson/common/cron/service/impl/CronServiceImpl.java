package com.benson.common.cron.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.benson.common.common.util.EnumUtil;
import com.benson.common.cron.entity.Cron;
import com.benson.common.cron.entity.CronInitEntity;
import com.benson.common.cron.entity.enums.TriggerStateEnum;
import com.benson.common.cron.mapper.CronDao;
import com.benson.common.cron.service.ICronService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.benson.common.cron.util.CacheInitCronData;
import com.benson.common.cron.util.QuartzUtils;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Function;

/**
 * <p>
 * 定时任务表 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2020-02-24
 */
@Service
public class CronServiceImpl extends ServiceImpl<CronDao, Cron> implements ICronService {

    @Autowired
    Scheduler scheduler;

    @Override
    public Function<Cron, Cron> initCron() {
        return cron -> {
            // 查询当前定时任务，状态
            TriggerStateEnum scheduleStatus = QuartzUtils.getScheduleStatus(scheduler, cron.getMark());
            cron.set("cronStatus", EnumUtil.toMap(scheduleStatus));
            // 查询当前Cron
            String currentCron = QuartzUtils.getScheduleCurrentCron(scheduler, cron.getMark());
            cron.set("currentCron", currentCron);
            // 获取原始Cron
            CronInitEntity cronInit = CacheInitCronData.getCronInit(cron.getMark());
            if (ObjectUtil.isNotNull(cronInit)) {
                cron.set("oldCron", cronInit.getCron());
                cron.set("cronIsEq", cron.getCron().trim().equals(cronInit.getCron().trim()));
            }
            return cron;
        };
    }

    @Override
    public Cron getCronByMark(String keyName) {
        try {
            Cron cron = getOne(new LambdaQueryWrapper<Cron>()
                    .eq(Cron::getMark, keyName)
            );
            return cron;
        } catch (Exception e) {

        }
        return null;
    }
}
