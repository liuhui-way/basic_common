package com.benson.common.cron.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.benson.common.common.exception.MyBaselogicException;
import com.benson.common.common.util.CommonUtil;
import com.benson.common.common.util.EnumUtil;
import com.benson.common.cron.entity.Cron;
import com.benson.common.cron.entity.CronInitEntity;
import com.benson.common.cron.entity.enums.TriggerStateEnum;
import com.benson.common.cron.service.ICronService;
import com.benson.common.cron.service.IQuartzService;
import com.benson.common.cron.util.CacheInitCronData;
import com.benson.common.cron.util.QuartzUtils;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 定时任务服务
 *
 * @author zhangby
 * @date 16/10/20 11:24 am
 */
@Service
public class QuartzServiceImpl implements IQuartzService {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private ICronService cronService;

    /**
     * 获取定时任务
     */
    @Override
    public List<Dict> getJobs(String keyword) {
        try {
            Set<TriggerKey> triggerKeys = scheduler.getTriggerKeys(GroupMatcher.anyGroup());
            // 返回数据
            return triggerKeys.stream()
                    .map(triggerKey ->
                        getFilterJob(keyword, getJobByJobName(triggerKey.getName()))
                    )
                    .filter(ObjectUtil::isNotNull)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return CollectionUtil.newArrayList();
    }

    // 过滤
    private Dict getFilterJob(String keyword, Dict job) {
        // 过滤
        if (job.getStr("cron").contains(keyword)) {
            return job;
        }
        // 过滤名称
        CronInitEntity cronConfig = CommonUtil.resolve(() -> Convert.convert(CronInitEntity.class, job.get("cronConfig")))
                .orElse(null);
        if (ObjectUtil.isNotNull(cronConfig)) {
            // 过滤
            if (cronConfig.getValue().contains(keyword) || cronConfig.getDescription().contains(keyword)) {
                return job;
            }
        }
        return null;
    }

    @Override
    public Dict getJobByJobName(String jobName) {
        // 定时任务
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName);
            if (ObjectUtil.isNotNull(triggerKey) && scheduler.checkExists(triggerKey)) {
                String keyName = triggerKey.getName();
                Dict paramsJob = Dict.create().set("mark", keyName);
                // 查询状态
                Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
                TriggerStateEnum stateEnum = EnumUtil.initEnum(TriggerStateEnum.class, triggerState.toString());
                // 返回结果
                paramsJob.set("cron", JSON.parseObject(JSON.toJSONString(scheduler.getTrigger(triggerKey)), Map.class).get("cronExpression"))
                        .set("cronConfig", CacheInitCronData.getCronInit(keyName))
                        .set("status", Dict.create()
                                .set("label", Optional.ofNullable(stateEnum).map(TriggerStateEnum::getLabel).orElse(""))
                                .set("value", Optional.ofNullable(stateEnum).map(TriggerStateEnum::getValue).orElse(""))
                        );
                // 查询数据库
                Cron sysCron = cronService.getCronByMark(keyName);
                if (ObjectUtil.isNotNull(sysCron)) {
                    paramsJob.set("dbCron", sysCron.getCron())
                            .set("dbCronId", sysCron.getId());
                }
                return paramsJob;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Dict> getJobsMonitor(String keyword) {
        try {
            Set<TriggerKey> triggerKeys = scheduler.getTriggerKeys(GroupMatcher.anyGroup());
            // 返回数据
            return triggerKeys.stream()
                    .map(triggerKey -> {
                        Dict job = getJobByJobName(triggerKey.getName());
                        if (ObjectUtil.isNotNull(job)) {
                            if (!job.getStr("cron").equals(job.getStr("dbCron"))) {
                                return getFilterJob(keyword, job);
                            }
                        }
                        return null;
                    })
                    .filter(ObjectUtil::isNotNull)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return CollectionUtil.newArrayList();
    }
}
